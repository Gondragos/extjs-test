(function() {
	Ext.namespace('App.grid');
	// Это крайне похоже на костыль... но это всего лишь доработка
	// Ext.grid.ActionColumn для своих нужд.
	App.grid.BtnActionColumn = Ext
			.extend(
					Ext.grid.Column,
					{
						header : '&#160;',
						actionIdRe : /x-action-col-(\d+)/,
						altText : ' ',
						constructor : function(cfg) {
							var me = this, items = cfg.items
									|| (me.items = [me]), l = items.length, i, item, additClass;

							App.grid.BtnActionColumn.superclass.constructor
									.call(me, cfg);
							me.renderer = function(v, meta) {
								// Allow a configured renderer to create initial
								// value (And set the other values in the
								// "metadata" argument!)
								v = Ext.isFunction(cfg.renderer) ? cfg.renderer
										.apply(this, arguments)
										|| '' : '';

								meta.css += ' x-action-col-cell';
								for (i = 0; i < l; i++) {
									item = items[i];
									additClass = (Ext.isFunction(item.getClass)
											? item.getClass.apply(item.scope
													|| this.scope || this,
													arguments)
											: '');
									if (additClass !== false) {
										v += '<button alt="'
												+ (item.altText || me.altText)
												+ '" class="btn action-icon x-action-col-'
												+ String(i)
												+ ' '
												+ (item.iconCls || '')
												+ ' '
												+ additClass
												+ '"'
												+ ((item.tooltip)
														? ' ext:qtip="'
																+ item.tooltip
																+ '"'
														: '') + '><span>'
												+ (item.text || item.tooltip)
												+ '</span></button>';
									}
								}
								return v;
							};
						},

						destroy : function() {
							delete this.items;
							delete this.renderer;
							return App.grid.BtnActionColumn.superclass.destroy
									.apply(this, arguments);
						},

						processEvent : function(name, e, grid, rowIndex,
								colIndex) {
							var m = e.getTarget().className
									.match(this.actionIdRe), item, fn;
							if (m && (item = this.items[parseInt(m[1], 10)])) {
								if (name == 'click') {
									(fn = item.handler || this.handler)
											&& fn.call(item.scope || this.scope
													|| this, grid, rowIndex,
													colIndex, item, e);
								} else if ((name == 'mousedown')
										&& (item.stopSelection !== false)) {
									return false;
								}
							}
							return App.grid.BtnActionColumn.superclass.processEvent
									.apply(this, arguments);
						}
					});
	Ext.reg('btnactioncolumn', App.grid.BtnActionColumn);
	Ext.grid.Column.types['btnactioncolumn'] = App.grid.BtnActionColumn;
})();