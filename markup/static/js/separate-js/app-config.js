(function() {
	Ext.namespace('App.Config');
	App.Config.title = 'История займов по заемщику';
	App.Config.Data = [
			[1, '14.01.2012', 60, '13.04.2012', 90000, 7, 108000, 1],
			[2, '13.02.2012', 10, '18.04.2012', 70000, 10, 84000, 0]];
	App.Config.Fields = [{
		name : 'id',
		type : 'integer'
	}, {
		name : 'datefrom',
		type : 'date',
		dateFormat : 'd.m.Y'
	}, {
		name : 'days-limit',
		type : 'integer'
	}, {
		name : 'dateto',
		type : 'date',
		dateFormat : 'd.m.Y'
	}, {
		name : 'loan',
		type : 'float'
	}, {
		name : 'percent',
		type : 'int'
	}, {
		name : 'sum',
		type : 'float'
	}, {
		name : 'status',
		type : 'int'
	}];
	App.Config.Docs = [['Расписка', 'png'], ['Пасспорт', 'jpeg'],
			['Инструкция', 'pdf'], ['Договор', 'doc'], ['Квитанция', 'txt'],
			['Документация', 'zip']];
	App.Config.DocsFields = [{
		name : 'file',
		type : 'string'
	}, {
		name : 'ext',
		type : 'string'
	}];
})();