(function() {
    Ext.namespace('App.grid');
    App.grid.DocsGrid = Ext.extend(Ext.grid.GridPanel, {
	initComponent : function() {
	    Ext.apply(this, {
		autoHeight : true,
		autoExpandColumn : 'file',
		enableHdMenu : false,
		width : 400,
		viewConfig : {
		    forceFit : true,
		    autoFill : true,
		    scrollOffset : 0
		},
		columns : [ {
			id: 'file',
		    header : 'Документ',
		    width : 150,
		    dataIndex : 'file'
		}, {
		    header : 'Расширение',
		    width : 85,
		    dataIndex : 'ext',
		    renderer: this.rendererExt
		}]
		});
	    App.grid.DocsGrid.superclass.initComponent.apply(this, arguments);
	},
	rendererExt: function(value, metaData, record, rowIndex, colIndex, store) {
		var classSuffix;
		value = Ext.util.Format.lowercase(value);
		switch (value) {
			case 'pdf':
				classSuffix='file-pdf-o';
				break;
			case 'doc':
			case 'docx':
				classSuffix='file-word-o';
				break;
			case 'ls':
			case 'xlsx':
				classSuffix='file-exel-o';
				break;
			case 'png':
			case 'jpg':
			case 'jpeg':
				classSuffix='file-image-o';
				break;
			case 'zip':
				classSuffix='file-archive-o';
				break;
			case 'txt':
				classSuffix='file-text-o';
				break;
			default:
				classSuffix='file-o';
				break;
		}
		return '<span class="ext-icon icon-'+classSuffix+'">'+value+'</span>';
	}
  });
  Ext.reg('app-grid-docsgrid', App.grid.DocsGrid);
})();