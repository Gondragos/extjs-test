(function() {
	Ext.namespace('App.grid');
	App.grid.LoanGrid = Ext.extend(Ext.grid.GridPanel, {
		initComponent : function() {
			Ext.apply(this, {
				autoHeight : true,
				autoExpandColumn : 'loan',
				enableHdMenu : false,
				width : 800,
				tbar : [{
					text : App.Lex('create_loan'),
					iconCls : 'btn-icon icon-edit',
					scale : 'small',
					handler : this.toolBarWindowSave,
					scope : this
				}, {
					text : App.Lex('view_loan'),
					iconCls : 'btn-icon icon-list-alt',
					scale : 'small',
					handler : this.toolBarWindowSave,
					scope : this
				}, {
					text : App.Lex('information'),
					iconCls : 'btn-icon icon-info-circle',
					scale : 'small',
					handler : this.toolBarWindowSave,
					scope : this
				}],
				viewConfig : {
					forceFit : true,
					autoFill : true,
					scrollOffset : 0
				},
				columns : [{
					header : App.Lex('date'),
					width : 85,
					dataIndex : 'datefrom',
					renderer : Ext.util.Format.dateRenderer('d.m.Y')
				}, {
					header : App.Lex('time-limit'),
					width : 85,
					dataIndex : 'days-limit',
					renderer : this.daysRenderer
				}, {
					header : App.Lex('maturity_date_reduct'),
					width : 85,
					dataIndex : 'dateto',
					renderer : Ext.util.Format.dateRenderer('d.m.Y')
				}, {
					id : 'loan',
					header : App.Lex('loan_body'),
					width : 85,
					flex : 5,
					dataIndex : 'loan',
					renderer : this.currencyFormat
				}, {
					header : App.Lex('%'),
					width : 40,
					dataIndex : 'percent'
				}, {
					header : App.Lex('Sum+%'),
					width : 85,
					dataIndex : 'sum',
					renderer : this.currencyFormat
				}, {
					header : App.Lex('status'),
					width : 60,
					dataIndex : 'status',
					renderer: this.statusRenderer
				}, {
					xtype : 'btnactioncolumn',
					width : 100,
					dataIndex : 'status',
					items : [{
						iconCls : 'btn-icon icon-print',
						tooltip : App.Lex('print'),
						handler : this.actionPageOpen
					}, {
						getClass : function(v, meta, rec) { // Or return a class
							// from a
							switch (rec.data.status) {
								case 0 :
									return 'icon-envelope-o'
									break;
								case 1 :
									return false;
									break;
							}
						},
						tooltip : App.Lex('docs_list'),
						handler : this.actionDocsShow,
						scope : this
					}, {
						getClass : function(v, meta, rec) { // Or return a class
							// from a
							switch (rec.data.status) {
								case 0 :
									return 'icon-remove'
									break;
								case 1 :
									return false;
									break;
							}
						},
						tooltip : App.Lex('remove'),
						handler : this.actionRemoveRow,
						scope : this
					}]
				}]
			});
			App.grid.LoanGrid.superclass.initComponent.apply(this, arguments);
		},
		daysRenderer: function(v) {
			return v +' '+App.util.Format.plural(v,App.Lex('day_plural'));
		},
		statusRenderer: function(v) {
			return v?App.Lex('loan_repaid'):App.Lex('loan_issued')
		},
		currencyFormat: function(v) {
			return Ext.util.Format.number(v, '0,0.00').replace(/,/g,'<span class="_phantomspace"></span>');
		},
		toolBarWindowSave : function(obj, e) {
			var window = Ext.create({
				xtype : 'app-window-simple',
				listeners : {
					success : {
						fn : function() {
							this.refresh();
						},
						scope : this
					}
				}
			});
			window.reset();
			window.show(e.target);
		},
		removeRow : function(index) {
			var store = this.getStore();
			store.removeAt(index);
		},
		removeRowPrompt : function(btn, index, response) {
			if (btn === 'ok' || btn === 'yes') {
				this.removeRow(index);
			}
		},
		actionRemoveRow : function(grid, rowIndex, colIndex) {
			var scope = arguments;
			Ext.Msg.show({
				title : App.Lex('action_prompt'),
				msg : App.Lex('prompt_row_remove'),
				fn : this.removeRowPrompt,
				scope : this,
				buttons : Ext.Msg.YESNO,
				value : rowIndex
			});
		},
		actionPageOpen : function(grid, rowIndex, colIndex) {
			var loan = grid.getStore().getAt(rowIndex).get('id');
			window.open('anypage.html?loan=' + loan);
		},
		actionDocsShow : function(grid, rowIndex, colIndex) {
			var docs = this.__generateDocs();
			var store = new Ext.data.ArrayStore({
				data : docs,
				fields : App.Config.DocsFields
			});
			var window = Ext.create({
				xtype : 'window',
				title : App.Lex('docs_list'),
				items : [{
					xtype : 'app-grid-docsgrid',
					store : store
				}]
			});
			window.show();
		},
		__generateDocs : function() {
			var docs = [];
			Ext.each(App.Config.Docs, function(doc) {
				if (Math.random() > 0.5) {
					docs.push(doc);
				}
			});
			return docs;
		}
	});
	Ext.reg('app-loan-grid', App.grid.LoanGrid);
})();