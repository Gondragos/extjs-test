(function() {
	Ext.namespace('App.util');
	Ext.namespace('App.Dictionary');
	App.Lex = App.util.Lex = function(key, value) {
		if (value != null && typeof (value) == "object") {
			var t = "" + (App.Dictionary[key] || key);
			for ( var k in value) {
				t = t.replace("{{" + k + "}}", value[k]);
			}
			return t;
		} else
			return App.Dictionary[key] || key;
	};
})();
Ext.onReady(function() {

	Date.dayNames = [App.Lex('sunday'), App.Lex('monday'), App.Lex('tuesday'),
			App.Lex('wednesday'), App.Lex('thursday'), App.Lex('friday'),
			App.Lex('saturday')];
	Date.monthNames = [App.Lex('january'), App.Lex('february'),
			App.Lex('march'), App.Lex('april'), App.Lex('may'),
			App.Lex('june'), App.Lex('july'), App.Lex('august'),
			App.Lex('september'), App.Lex('october'), App.Lex('november'),
			App.Lex('december')];
	Ext.apply(Ext.grid.GridView.prototype, {
		sortAscText : App.Lex('ext_sortasc'),
		sortDescText : App.Lex('ext_sortdesc'),
		lockText : App.Lex('ext_column_lock'),
		unlockText : App.Lex('ext_column_unlock'),
		columnsText : App.Lex('ext_columns'),
		emptyText : App.Lex('ext_emptymsg')
	});
	Ext.apply(Ext.DatePicker.prototype, {
		todayText : App.Lex('today'),
		todayTip : App.Lex('ext_today_tip'),
		minText : App.Lex('ext_mindate'),
		maxText : App.Lex('ext_maxdate'),
		monthNames : Date.monthNames,
		dayNames : Date.dayNames,
		nextText : App.Lex('ext_nextmonth'),
		prevText : App.Lex('ext_prevmonth'),
		monthYearText : App.Lex('ext_choosemonth')
	});

	Ext.MessageBox.buttonText = {
		yes : App.Lex('yes'),
		no : App.Lex('no'),
		ok : App.Lex('ok'),
		cancel : App.Lex('cancel')
	};
	Ext.apply(Ext.PagingToolbar.prototype, {
		afterPageText : App.Lex('ext_afterpage'),
		beforePageText : App.Lex('ext_beforepage'),
		displayMsg : App.Lex('ext_displaying'),
		emptyMsg : App.Lex('ext_emptymsg'),
		firstText : App.Lex('ext_first'),
		prevText : App.Lex('ext_prev'),
		nextText : App.Lex('ext_next'),
		lastText : App.Lex('ext_last'),
		refreshText : App.Lex('ext_refresh')
	});
	Ext.apply(Ext.Updater.prototype, {
		text : App.Lex('loading')
	});
	Ext.apply(Ext.LoadMask.prototype, {
		msg : App.Lex('loading')
	});
	Ext.apply(Ext.layout.BorderLayout.SplitRegion.prototype, {
		splitTip : App.Lex('ext_splittip')
	});
	Ext.apply(Ext.form.BasicForm.prototype, {
		waitTitle : App.Lex('please_wait')
	});
	Ext.apply(Ext.form.ComboBox.prototype, {
		loadingText : App.Lex('loading')
	});
	Ext.apply(Ext.form.Field.prototype, {
		invalidText : App.Lex('ext_invalidfield')
	});
	Ext.apply(Ext.form.TextField.prototype, {
		minLengthText : App.Lex('ext_minlenfield'),
		maxLengthText : App.Lex('ext_maxlenfield'),
		invalidText : App.Lex('ext_invalidfield'),
		blankText : App.Lex('field_required')
	});
	Ext.apply(Ext.form.NumberField.prototype, {
		minText : App.Lex('ext_minvalfield'),
		maxText : App.Lex('ext_maxvalfield'),
		nanText : App.Lex('ext_nanfield')
	});
	Ext.apply(Ext.form.DateField.prototype, {
		disabledDaysText : App.Lex('disabled'),
		disabledDatesText : App.Lex('disabled'),
		minText : App.Lex('ext_datemin'),
		maxText : App.Lex('ext_datemax'),
		invalidText : App.Lex('ext_dateinv')
	});
	Ext.apply(Ext.form.VTypes, {
		emailText : App.Lex('ext_inv_email'),
		urlText : App.Lex('ext_inv_url'),
		alphaText : App.Lex('ext_inv_alpha'),
		alphanumText : App.Lex('ext_inv_alphanum')
	});
	Ext.apply(Ext.grid.GroupingView.prototype, {
		emptyGroupText : App.Lex('ext_emptygroup'),
		groupByText : App.Lex('ext_groupby'),
		showGroupsText : App.Lex('ext_showgroups')
	});
	Ext.apply(Ext.grid.PropertyColumnModel.prototype, {
		nameText : App.Lex('name'),
		valueText : App.Lex('value')
	});
	Ext.apply(Ext.form.CheckboxGroup.prototype, {
		blankText : App.Lex('ext_checkboxinv')
	});
	Ext.apply(Ext.form.RadioGroup.prototype, {
		blankText : App.Lex('ext_checkboxinv')
	});
	Ext.apply(Ext.form.TimeField.prototype, {
		minText : App.Lex('ext_timemin'),
		maxText : App.Lex('ext_timemax'),
		invalidText : App.Lex('ext_timeinv')
	});
});