(function() {
	Ext.namespace('App.util.Format');
	App.util.Format = function() {
		return {
			plural : function(value, onePostfix, somePostfix, manyPostfix) {
				if (Ext.isArray(onePostfix)) {
					return this.plural(value, onePostfix[0], onePostfix[1],
							onePostfix[2]);
				}
				var digits = [];
				if (!onePostfix)
					return;
				if (typeof somePostfix == "undefined")
					somePostfix = onePostfix;
				if (typeof manyPostfix == "undefined")
					manyPostfix = somePostfix;
				digits[0] = value % 10;
				digits[1] = Math.floor((value % 100) / 10);
				if (digits[1] == 1) {
					return manyPostfix;
				} else if (digits[0] == 1) {
					return onePostfix;
				} else if (digits[0] < 5 && digits[0] > 0) {
					return somePostfix;
				} else {
					return manyPostfix;
				}
			}
		}
	}();
})();