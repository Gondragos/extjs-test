(function() {
	Ext.namespace('App.window');
	App.window.Simple = Ext.extend(App.WindowForm, {
		url : '/index.html',
		fields : [{
			fieldLabel : App.Lex('one_field'),
			xtype : 'textfield',
			name : 'q',
			anchor : '100%'
		}],
		initComponent : function() {
			this.buttons = [{
				text : this.saveBtnText || App.Lex('save'),
				cls : 'primary-button',
				scope : this,
				handler : this.submit
			}];
			App.window.Simple.superclass.initComponent.apply(this, arguments);
		}
	});
	Ext.reg('app-window-simple', App.window.Simple);
})();