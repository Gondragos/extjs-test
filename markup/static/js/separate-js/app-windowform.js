(function() {
	Ext.namespace('App');
	App.WindowForm = Ext.extend(Ext.Window,{
		initComponent: function() {
			Ext.applyIf(this,{
		        modal: false
		        ,layout: 'auto'
		        ,closeAction: 'hide'
		        ,shadow: true
		        ,resizable: true
		        ,collapsible: true
		        ,maximizable: true
		        ,autoHeight: false
		        ,autoScroll: true
		        ,width: 400
		        ,buttons: [{
		            text: this.cancelBtnText || App.Lex('cancel')
		            ,scope: this
		            ,handler: function() { this.closeAction !== 'close' ? this.hide() : this.close(); }
		        },{
		            text: this.saveBtnText || App.Lex('save')
		            ,cls: 'primary-button'
		            ,scope: this
		            ,handler: this.submit
		        }]
		        ,record: {}
		        ,keys: [{
		            key: Ext.EventObject.ENTER
		            ,fn: this.submit
		            ,scope: this
		        }]
		    },this.initialConfig);
			App.WindowForm.superclass.initComponent.apply(this,arguments);
			this.addEvents({
		        success: true
		        ,failure: true
		        ,beforeSubmit: true
		    });
		    this._loadForm();
		    this.on('show',function() {
		        if (this.blankValues) { this.fp.getForm().reset(); }
		        this.syncSize();
		        this.focusFirstField();
		    },this);
		    this.on('afterrender', function() {
		        this.originalHeight = this.el.getHeight();
		        this.toolsHeight = this.originalHeight - this.body.getHeight() + 50;
		        this.resizeWindow();
		    });
		    Ext.EventManager.onWindowResize(this.resizeWindow, this);
		},
	    _loadForm: function() {
	        if (this.checkIfLoaded(this.record || null)) { return false; }
			
	        var r = this.record;
	        /* set values here, since setValue after render seems to be broken */
	        if (this.fields) {
	            var l = this.fields.length;
	            for (var i=0;i<l;i++) {
	                var f = this.fields[i];
	                if (r[f.name]) {
	                    if (f.xtype == 'checkbox' || f.xtype == 'radio') {
	                        f.checked = r[f.name];
	                    } else {
	                        f.value = r[f.name];
	                    }
	                }
	            }
	        }
	        this.fp = this.createForm({
	            url: this.url
	            ,baseParams: this.baseParams || { action: this.action || '' }
	            ,items: this.fields || []
	            ,method:this.method
	        });
	        var w = this;
	        this.fp.getForm().items.each(function(f) {
	            f.on('invalid', function(){
	                w.doLayout();
	            });
	        });
	        this.renderForm();
	    }

	    ,focusFirstField: function() {
	        if (this.fp && this.fp.getForm() && this.fp.getForm().items.getCount() > 0) {
	            var fld = this.findFirstTextField();
	            if (fld) { fld.focus(false,200); }
	        }
	    }
	    ,findFirstTextField: function(i) {
	        i = i || 0;
	        var fld = this.fp.getForm().items.itemAt(i);
	        if (!fld) return false;
	        if (fld.isXType('combo') || fld.isXType('checkbox') || fld.isXType('radio') || fld.isXType('displayfield') || fld.isXType('statictextfield') || fld.isXType('hidden')) {
	            i = i+1;
	            fld = this.findFirstTextField(i);
	        }
	        return fld;
	    }
		
	    ,submit: function(close) {
	        close = close === false ? false : true;
	        var f = this.fp.getForm();
	        if (f.isValid() && this.fireEvent('beforeSubmit',f.getValues())) {
	            f.submit({
	                waitMsg: App.Lex('saving')
	                ,scope: this
	                ,failure: function(frm,a) {
	                    if (this.fireEvent('failure',{f:frm,a:a})) {
	                       // Ext.Msg.alert('Ошибка');
	                    }
	                    if (close) { this.closeAction !== 'close' ? this.hide() : this.close(); }
	                    this.doLayout();
	                }
	                ,success: function(frm,a) {
	                    if (this.success) {
	                        Ext.callback(this.success,this.scope || this,[frm,a]);
	                    }
	                    this.fireEvent('success',{f:frm,a:a});
	                    if (close) { this.closeAction !== 'close' ? this.hide() : this.close(); }
	                    this.doLayout();
	                }
	            });
	        }
	    }
		
	    ,createForm: function(config) {
	        Ext.applyIf(this,{
	            formFrame: true
	            ,border: false
	            ,bodyBorder: false
	            ,autoHeight: true
	        });
	        config = config || {};
	        Ext.applyIf(config,{
	            labelAlign: this.labelAlign || 'top'
	            ,labelWidth: this.labelWidth || 100
	            ,labelSeparator: this.labelSeparator || ''
	            ,frame: this.formFrame
	            ,border: this.border
	            ,bodyBorder: this.bodyBorder
	            ,autoHeight: this.autoHeight
	            ,anchor: '100% 100%'
	            //,errorReader: 
	            ,defaults: this.formDefaults || {
	                msgTarget: this.msgTarget || 'under'
	            }
	            ,url: this.url
	            ,method: this.method
	            ,baseParams: this.baseParams || {}
	            ,fileUpload: this.fileUpload || false
	        });
	        return new Ext.FormPanel(config);
	    }

	    ,renderForm: function() {
	        this.add(this.fp);
	    }
		
	    ,checkIfLoaded: function(r) {
	        r = r || {};
	        if (this.fp && this.fp.getForm()) { /* so as not to duplicate form */
	            this.fp.getForm().reset();
	            this.fp.getForm().setValues(r);
	            return true;
	        }
	        return false;
	    }
	    
	    ,setValues: function(r) {
	        if (r === null) { return false; }
	        this.fp.getForm().setValues(r);
	    }
	    ,reset: function() {
	        this.fp.getForm().reset();
	    }

	    ,resizeWindow: function(){
	        var viewHeight = Ext.getBody().getViewSize().height;
	        var el = this.fp.getForm().el;
	        if(viewHeight < this.originalHeight){
	            el.setStyle('overflow-y', 'scroll');
	            el.setHeight(viewHeight - this.toolsHeight);
	        }else{
	            el.setStyle('overflow-y', 'auto');
	            el.setHeight('auto');
	        }
	    }
	});
	Ext.reg('windowform',App.WindowForm);
})();