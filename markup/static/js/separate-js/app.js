Ext.onReady(function() {
    Ext.QuickTips.init();
    var store = new Ext.data.ArrayStore({
		data : App.Config.Data,
		fields : App.Config.Fields
    });
    var TestGrid = new App.grid.LoanGrid({
		title : App.Config.title,
		store: store,
		renderTo : 'js-test-app'
    });
});